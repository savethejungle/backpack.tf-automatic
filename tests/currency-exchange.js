const backpacktf = require("../app/backpacktf");
const expect = require("chai").expect;

describe("currency-exchange", function() {
    it("should not change anything if keys are equal", function() {
        const ours = {metal: 0, keys: 1};
        const theirs = {metal: 0, keys: 1};
        const options = {
            mayExchangeToKeys: true,
            mayExchangeToMetal: true,
            keysAverage: 28.33
        };

        const {keysOk: keysOk, metalOk: metalOk} = backpacktf.exchangeCurrencies(ours, theirs, options);

        expect(keysOk).to.be.true;
        expect(metalOk).to.be.true;
        expect(ours.metal).to.equal(0);
        expect(theirs.metal).to.equal(0);
        expect(ours.keys).to.equal(1);
        expect(theirs.keys).to.equal(1);
    });

    it("should exchange metal according to exchange rate", function() {
        const ours = {metal: 0, keys: 1};
        const theirs = {metal: 28.33, keys: 0};
        const options = {
            mayExchangeToKeys: true,
            mayExchangeToMetal: true,
            keysAverage: 28.33
        };

        const {keysOk: keysOk, metalOk: metalOk} = backpacktf.exchangeCurrencies(ours, theirs, options);

        expect(keysOk).to.be.true;
        expect(metalOk).to.be.true;
        expect(ours.metal).to.equal(0);
        expect(theirs.metal).to.equal(0);
        expect(ours.keys).to.equal(1);
        expect(theirs.keys).to.equal(0);
    });

    it("should not exchange metal if exchange is disabled", function() {
        const ours = {metal: 0, keys: 1};
        const theirs = {metal: 28.33, keys: 0};
        const options = {
            mayExchangeToKeys: false,
            mayExchangeToMetal: true,
            keysAverage: 28.33
        };

        const {keysOk: keysOk, metalOk: metalOk} = backpacktf.exchangeCurrencies(ours, theirs, options);

        expect(keysOk).to.be.false;
        expect(metalOk).to.be.true;
        expect(ours.metal).to.equal(0);
        expect(theirs.metal).to.equal(28.33);
        expect(ours.keys).to.equal(1);
        expect(theirs.keys).to.equal(0);
    });
});