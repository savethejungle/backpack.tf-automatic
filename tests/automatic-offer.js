const AutomaticOffer = require("../app/automatic-offer");
const backpacktf = require("../app/backpacktf");
const expect = require("chai").expect;
const printf = require("printf");

const automatic = {
    manager: {
        on: (string, func) => {
            console.log(string);
            console.log(func);
        }
    },
    currencyAvg: () => 28.33,
    mayExchangeToCurrency: (string) => true
};

describe("automatic-offer", function() {
    it("should not be affected by 1.3.0 currency vulnerability", function() {
        // todo: finish this. kinda taxing to create all the trade state
        backpacktf.register(automatic);

        const tradeoffer = {
            itemsToGive: [
                {
                    market_hash_name: "Mann Co. Supply Crate Key",
                    name_color: "76D600",
                    descriptions: []
                },
                {
                    market_hash_name: "Refined Metal",
                    name_color: "76D600",
                    descriptions: []
                },
            ],
            itemsToReceive: [
                {
                    market_hash_name: "Mann Co. Supply Crate Key",
                    name_color: "76D600",
                    descriptions: []
                },
            ]
        };

        const offer = new AutomaticOffer(tradeoffer);
        offer.log = (level, msg) => printf("[%s] %s", level, msg);

        const listings = [
            {
                currencies: {
                    keys: 1,
                    metal: 3
                }
            }
        ];

        console.log(offer);
        const result = backpacktf.handleSellOrder(offer, listings);
        console.log(offer);

        console.log(result);
    });
});